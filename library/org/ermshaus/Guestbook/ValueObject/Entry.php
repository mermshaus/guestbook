<?php

namespace org\ermshaus\Guestbook\ValueObject;

class Entry
{
    public $date;
    public $author;
    public $message;
}
