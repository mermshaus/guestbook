<?php

namespace org\ermshaus\Guestbook;

use \org\ermshaus\Guestbook\Adapter\AdapterInterface,
    \org\ermshaus\Guestbook\ValueObject\Entry;

class GuestbookModel
{
    /**
     *
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     *
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter = null)
    {
        $this->adapter = $adapter;
    }

    /**
     *
     * @return array Array of Entry objects
     */
    public function getEntries()
    {
        if (!$this->adapter instanceof AdapterInterface) {
            throw new GuestbookException('No adapter set');
        }

        return $this->adapter->read();
    }

    /**
     *
     * @param Entry $entry
     */
    public function addEntry(Entry $entry)
    {
        if (!$this->adapter instanceof AdapterInterface) {
            throw new GuestbookException('No adapter set');
        }

        $this->adapter->write($entry);
    }

    public function addEntryFromPOST()
    {
        $errors = array();

        // Sanitize and validate

        $_POST['author'] = (isset($_POST['author']))
                ? trim((string) $_POST['author']) : '';
        $_POST['message'] = (isset($_POST['author']))
                ? trim((string) $_POST['message']) : '';

        if ($_POST['author'] === '') {
            $errors[] = 'Please enter your name.';
        }

        if ($_POST['message'] === '') {
            $errors[] = 'Please enter a message.';
        }

        if (empty($errors)) {
            $entry = new Entry();
            $entry->author  = $_POST['author'];
            $entry->message = $_POST['message'];

            $this->addEntry($entry);

            unset($_POST['author']);
            unset($_POST['message']);
        }

        return $errors;
    }

    /**
     *
     * @return AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     *
     * @param AdapterInterface $adapter
     */
    public function setAdapter(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }
}
