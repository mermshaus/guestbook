<?php

namespace org\ermshaus\Guestbook\Adapter\PDO;

use \org\ermshaus\Guestbook\Adapter\AbstractAdapter,
    \org\ermshaus\Guestbook\Adapter\AdapterException,
    \org\ermshaus\Guestbook\ValueObject\Entry,
    \PDO;

class PDOAdapter extends AbstractAdapter
{
    protected $pdoOptions;
    protected $tableName;

    protected $pdo;

    public function __construct(array $pdoOptions, $tableName = 'guestbook')
    {
        $pdoOptionsDefault = array(
            'dsn'           => '',
            'username'      => null,
            'password'      => null,
            'driverOptions' => array()
        );

        $this->pdoOptions = array_merge($pdoOptionsDefault, $pdoOptions);
        $this->tableName  = $tableName;
    }

    public function initialize()
    {
        $this->pdo = new PDO(
                $this->pdoOptions['dsn'],
                $this->pdoOptions['user'],
                $this->pdoOptions['password'],
                $this->pdoOptions['driverOptions']);
    }

    public function read()
    {
        $entries = array();

        $query = "
            SELECT
                    `date_added`,
                    `author`,
                    `message`
            FROM
                    `" . $this->tableName . "`
            ORDER BY
                    `date_added` DESC";

        foreach ($this->pdo->query($query) as $row) {
            $entry = new Entry();
            $entry->author  = $row['author'];
            $entry->date    = $row['date_added'];
            $entry->message = $row['message'];
            $entries[] = $entry;
        }

        return $entries;
    }

    public function write(Entry $entry)
    {
        $query = "
            INSERT INTO
                    `" . $this->tableName . "`
                (
                    `id`,
                    `date_added`,
                    `author`,
                    `message`
                )
            VALUES
                (
                    NULL,
                    UTC_TIMESTAMP(),
                    " . $this->pdo->quote($entry->author) . ",
                    " . $this->pdo->quote($entry->message) . "
                )";

        $success = $this->pdo->query($query);

        if ($success === false) {
            throw new AdapterException(sprintf(
                    'The following query failed: %s',
                    $query));
        }
    }
}
