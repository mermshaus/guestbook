<?php

namespace org\ermshaus\Guestbook\Adapter\Plaintext;

use \org\ermshaus\Guestbook\Adapter\AbstractAdapter,
    \org\ermshaus\Guestbook\ValueObject\Entry;

class PlaintextAdapter extends AbstractAdapter
{
    protected $storageFilepath = '';

    public function __construct($storageFilepath)
    {
        $this->storageFilepath = $storageFilepath;
    }

    public function getStorageFilepath()
    {
        return $this->storageFilepath;
    }

    public function setStorageFilepath($storageFilepath)
    {
        $this->storageFilepath = $storageFilepath;
    }

    public function initialize()
    {
        if (!file_exists($this->storageFilepath)
                && !touch($this->storageFilepath)
        ) {
            throw new InvalidArgumentException(sprintf(
                    'Storage file %s does not exist and could not be created',
                    $this->storageFilepath));
        }
    }

    public function read()
    {
        $data = file_get_contents($this->storageFilepath);

        if ($data === false) {
            throw new AdapterException(sprintf(
                    'Unable to read from storage file %s',
                    $this->storageFilepath));
        }

        $entries = ($data === '') ? array() : unserialize($data);

        if ($entries === false) {
            throw new AdapterException(sprintf(
                    'Unable to unserialize data from storage file %s',
                    $this->storageFilepath));
        }

        if (!is_array($entries)) {
            throw new AdapterException(sprintf(
                    'Data from storage file %s is of wrong format',
                    $this->storageFilepath));
        }

        foreach ($entries as $entry) {
            if (!$entry instanceof Entry) {
                throw new AdapterException(sprintf(
                   'At least one entry from storage file %s is of wrong format',
                   $this->storageFilepath));
            }
        }

        return $entries;
    }

    /**
     *
     * @param Entry $entry
     */
    public function write(Entry $entry)
    {
        $entries = $this->read();

        // Generate UTC date
        $entry->date = gmdate('Y-m-d H:i:s');

        $entries[] = $entry;

        usort($entries, function (Entry $a, Entry $b) {
            return ($a->date > $b->date) ? -1 : 1;
        });

        $success = file_put_contents($this->storageFilepath,
                serialize($entries), LOCK_EX);

        if ($success === false) {
            throw new AdapterException(sprintf(
                    'Unable to write to storage file %s',
                    $this->storageFilepath));
        }
    }
}
