<?php

namespace org\ermshaus\Guestbook\Adapter;

use \org\ermshaus\Guestbook\ValueObject\Entry;

interface AdapterInterface
{
    public function initialize();

    public function write(Entry $entry);

    /**
     *
     * @return array
     */
    public function read();
}
