<?php

namespace org\ermshaus\Guestbook;

/**
 *
 * @see http://www.phpforum.de/forum/showthread.php?t=217421
 */
function sanitizeMagicQuotes()
{
    // Magic quotes
    // PHP6 safe
    if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
        if (!function_exists('array_stripslashes')) {
            // man könnte unter PHP5+6 auch mit
            // array_walk_recursive() arbeiten
            function array_stripslashes(&$var) {
                if(is_string($var))
                    $var = stripslashes($var);
                else
                    if(is_array($var))
                        foreach($var AS $key => $value)
                            array_stripslashes($var[$key]);
            }
        }

        // die wichtigsten
        array_stripslashes($_GET);
        array_stripslashes($_POST);
        array_stripslashes($_COOKIE);

        // seltener notwendig
        #array_stripslashes($_REQUEST);
        #array_stripslashes($_FILES);
    }
}

/**
 * Convert a date from UTC to the script's timezone
 *
 * There has to be a more elegant way to do this.
 *
 * @param string $utcDate
 * @return string
 */
function utcTolocalDate($utcDate)
{
    $tz = date_default_timezone_get();
    date_default_timezone_set('UTC');
    $utc_timestamp = strtotime($utcDate);
    date_default_timezone_set($tz);

    return date('Y-m-d H:i:s', $utc_timestamp);
}


function escape($data)
{
    return \htmlspecialchars($data, \ENT_QUOTES, 'UTF-8');
}
