<?php

use \org\ermshaus\Guestbook\Adapter\Plaintext\PlaintextAdapter,
    \org\ermshaus\Guestbook\Adapter\PDO\PDOAdapter,
    \org\ermshaus\Guestbook\GuestbookModel,
    \org\ermshaus\Guestbook as G,
    \org\ermshaus\Guestbook\ValueObject\Entry;

// The "framework" is too thin to put these functions elsewhere
require_once './library/org/ermshaus/Guestbook/inc.helpers.php';

error_reporting(-1);

header('content-type: text/html; charset=utf-8');

// All dates are saved as UTC, so it is a good idea to set a local timezone to
// format the output
date_default_timezone_set('Europe/Berlin');

// Magic quotes wizardry
G\sanitizeMagicQuotes();

// Autoloading for namespaces
spl_autoload_register(function ($class) {
    require_once './library/' . str_replace('\\', '/', $class) . '.php';
});

/* **************************** Change adapter here */

$adapter = new PlaintextAdapter('./data/gb-data.txt');

//$adapter = new PDOAdapter(array(
//    'dsn'      => 'mysql:dbname=test;host=localhost',
//    'user'     => '',
//    'password' => '',
//    'driverOptions' => array(
//        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
//    )
//));

/* ************************************************ */

$adapter->initialize();

// Determine active action
$action = (isset($_POST['action']))
        ? trim((string) $_POST['action']) : 'index';

if (!in_array($action, array('index', 'add'))) {
    $action = 'index';
}

// Template variables
$t = array();
$t['action'] = $action;
$t['errors'] = array();

// This is the "controller"
switch ($action) {
    case 'add':
        // Try to add a new entry
        $model = new GuestbookModel($adapter);
        $t['errors']  = $model->addEntryFromPOST();
        $t['entries'] = $model->getEntries();
        break;

    case 'index':
    default:
        // List existing entries
        $model = new GuestbookModel($adapter);
        $t['entries'] = $model->getEntries();
        break;
}

// Output
?><!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Guestbook</title>
    <link rel="stylesheet" type="text/css" href="styles.css" />
</head>

<body>

    <h1>Guestbook</h1>

    <?php if ($t['action'] === 'add'): ?>

        <?php foreach ($t['errors'] as $error): ?>

        <p><?php echo G\escape($error); ?></p>

        <?php endforeach; ?>

        <?php if (empty($t['errors'])): ?>

        <p>Entry added successfully. Thanks!</p>

        <?php endif; ?>

    <?php endif; ?>

    <form method="post" action="">
        <fieldset>
            <legend>Add entry</legend>

            <p>
                Name:
                <input type="text" name="author" value="<?php
                    echo G\escape((isset($_POST['author'])
                            ? $_POST['author'] : ''));
                ?>" />
            </p>

            <p>
                Message:
                <textarea cols="40" rows="10" name="message"><?php
                    echo G\escape((isset($_POST['message'])
                            ? $_POST['message'] : ''));
                ?></textarea>
            </p>

            <p>
                <input type="hidden" name="action" value="add" />
                <input type="submit" value="Send" />
            </p>
        </fieldset>
    </form>

    <?php if (empty($t['entries'])): ?>
    <p>The guestbook does not contain any entries.</p>
    <?php endif; ?>

    <?php foreach ($t['entries'] as $entry): ?>
    <div class="entry">
        <p class="info">
            <?php echo G\escape($entry->author); ?> wrote on
            <?php echo G\escape(date('M d, Y \a\t H:ia',
                    strtotime(G\utcTolocalDate($entry->date)))); ?>:
        </p>

        <p><?php echo nl2br(G\escape($entry->message)); ?>

    </div>
    <?php endforeach; ?>

</body>

</html>
